<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$GLOBALS["arrFilterMainTheme"] = array("PROPERTY_MAIN_VALUE" => 1);
$GLOBALS["arrFilterMain"] = array("PROPERTY_MAIN_VALUE" => 1);
?>
<main class="website-workarea">
    <section class="banner banner_before54" style="background-image: url('local/templates/yanicode/assets/images/banner_services.jpg');">
        <div class="banner-wrapper">
            <div class="container">
                <div class="banner__content">
                    <p><b class="text_gold">МЫ — ДИЗАЙН СТУДИЯ: </b>и у нас есть собственные проекты.
                        Обратитесь к нам, мы будем рады помочь с дизайном и разработкой</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="stages">
                <div class="stages__item">
                    <div class="stages__step">01</div>
                    <div class="stages__desc-step">Цель проекта</div>
                </div>
                <div class="stages__item">
                    <div class="stages__step">02</div>
                    <div class="stages__desc-step">Исследование</div>
                </div>
                <div class="stages__item">
                    <div class="stages__step">03</div>
                    <div class="stages__desc-step">Мозговой штурм</div>
                </div>
                <div class="stages__item">
                    <div class="stages__step">04</div>
                    <div class="stages__desc-step">Анализ</div>
                </div>
                <div class="stages__item">
                    <div class="stages__step">05</div>
                    <div class="stages__desc-step">Реализация</div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <h1 class="container-title">
            НАШИ УСЛУГИ
        </h1>
        <div class="services-cover">
            <div class="services">
                <h2 class="services__title">
                    DIGITAL
                </h2>
                <div class="services-category">
                    <div class="services__item" data-popup="services-popup">Дизайн бренда</div>
                    <div class="services__item" data-popup="services-popup">Целевая аудитория</div>
                    <div class="services__item" data-popup="services-popup">Концепция дизайна</div>
                </div>
                <div class="services-category">
                    <a href="123/" class="services__item" data-popup="services-popup">Нейминг</a>
                    <div class="services__item" data-popup="services-popup">Дизайн логотипа</div>
                    <div class="services__item" data-popup="services-popup">Регистрация товарного знака</div>
                    <div class="services__item" data-popup="services-popup">Фирменный дизайн</div>
                </div>
                <div class="services-category">
                    <div class="services__item" data-popup="services-popup">Дизайн-концепт упаковки</div>
                    <div class="services__item" data-popup="services-popup">Дизайн упаковки</div>
                    <div class="services__item" data-popup="services-popup">Digital дизайн</div>
                    <div class="services__item" data-popup="services-popup">Внедрение</div>
                </div>
            </div>

            <div class="services">
                <h2 class="services__title">
                    ДИЗАЙН
                </h2>
                <div class="services-category">
                    <div class="services__item" data-popup="services-popup">Дизайн бренда</div>
                    <div class="services__item" data-popup="services-popup">Целевая аудитория</div>
                    <div class="services__item" data-popup="services-popup">Дизайн стратегия</div>
                    <div class="services__item" data-popup="services-popup">Креативный концепт</div>
                    <div class="services__item" data-popup="services-popup">Тактические рекламные кампании</div>
                    <div class="services__item" data-popup="services-popup">Медипланирование</div>
                </div>
                <div class="services-category">
                    <div class="services__item" data-popup="services-popup">OUTSTAFF Дизайн</div>
                    <div class="services__item" data-popup="services-popup">Дизайн продукта</div>
                    <div class="services__item" data-popup="services-popup">Дизайн</div>
                </div>
                <div class="services-category">
                    <div class="services__item" data-popup="services-popup">Моушн графика</div>
                    <div class="services__item" data-popup="services-popup">3D графика</div>
                </div>
            </div>

<!--            <div class="services">-->
<!--                <h2 class="services__title">-->
<!--                    DIGITAL БРЕНДИНГ-->
<!--                </h2>-->
<!--                <div class="services-category">-->
<!--                    <div class="services__item" data-popup="services-popup">Аудит ситуации</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Позиционирование</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Фирменный стиль</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Разработка уникального контента</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Фотосессия</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Видеовизитка</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Оформление соц сетей</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Контент-план</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Сайт</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Продвижение</div>-->
<!--                </div>-->
<!--            </div>-->

<!--            <div class="services">-->
<!--                <h2 class="services__title">-->
<!--                    ЛИЧНЫЙ БРЕНД-->
<!--                </h2>-->
<!--                <div class="services-category">-->
<!--                    <div class="services__item" data-popup="services-popup">Аудит ситуации</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Целевая аудитория</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Позиционирование</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Личная легенда </div>-->
<!--                    <div class="services__item" data-popup="services-popup">Стратегия продвижения</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Оформление соцсетей</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Контент-план</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Фотосессия</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Подбор специалистов</div>-->
<!--                    <div class="services__item" data-popup="services-popup">Сайт</div>-->
<!--                </div>-->
<!--            </div>-->
        </div>
        <div class="button-wrapper  button-wrapper_center">
            <div class="button button_gold button-open-calculate-project" data-popup="calculate-project-popup">
                СВЯЗАТЬСЯ С НАМИ
            </div>
        </div>
    </div>


    <template id="popup-services">
        <div class="popup-services-cover">
            <div class="popup-services__title">
                БРЕНДИНГ
            </div>
            <div class="popup-services__category">
                Аудит бренда
            </div>
            <div class="popup-services__desc">
                <p>
                    Это диагностика его ключевых параметров и характеристик для понимания дальнейших путей развития. Прежде всего, он дает ответ на вопрос: что сохранить и что изменить при ребрендинге.
                </p>
                <p>
                    В базовой версии аудита мы анализируем восприятие бренда; целостность его образа в различных точках контакта с потребителем; заметность бренда в конкурентном окружении. Особое внимание мы уделяем анализу фирменного стиля и коммуникаций, поскольку это именно та сфера, в которой нам предстоит работать. По итогам аудита мы даем рекомендации по возможным направлениям развития фирменного стиля бренда.
                </p>
                <p>
                    Если на момент проведения аудита маркетинговая стратегия уже сформирована, мы сопоставляем цели и планы компании с текущим образом бренда.
                </p>
                <p>
                    В расширенных версиях может быть проведен анализ ассортиментного портфеля, пути потребителя, представленности в тех или иных каналах продаж и коммуникаций и т.д. Набор параметров для анализа определяется индивидуально для каждого проекта.
                </p>
            </div>
        </div>
    </template>
    <template id="popup-calculate-project">
        <div class="js-validated-form">
            <?$APPLICATION->IncludeComponent(
                "bitrix:form",
                ".default",
                Array(
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_TIME" => "3600",
                    "CACHE_TYPE" => "A",
                    "CHAIN_ITEM_LINK" => "",
                    "CHAIN_ITEM_TEXT" => "",
                    "COMPONENT_TEMPLATE" => ".default",
                    "EDIT_ADDITIONAL" => "N",
                    "EDIT_STATUS" => "Y",
                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                    "NAME_TEMPLATE" => "",
                    "NOT_SHOW_FILTER" => array(0=>"",1=>"",),
                    "NOT_SHOW_TABLE" => array(0=>"",1=>"",),
                    "RESULT_ID" => $_REQUEST[RESULT_ID],
                    "SEF_MODE" => "N",
                    "SHOW_ADDITIONAL" => "N",
                    "SHOW_ANSWER_VALUE" => "N",
                    "SHOW_EDIT_PAGE" => "N",
                    "SHOW_LIST_PAGE" => "N",
                    "SHOW_STATUS" => "Y",
                    "SHOW_VIEW_PAGE" => "N",
                    "START_PAGE" => "list",
                    "SUCCESS_URL" => "",
                    "USE_EXTENDED_ERRORS" => "N",
                    "VARIABLE_ALIASES" => array("action"=>"action",),
                    "WEB_FORM_ID" => "11"
                )
            );?>
        </div>
    </template>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
